require "rubygems"
require "mysql"
require 'csv'

begin
	begin
		db = Mysql.new('localhost','root','toor','test')
	rescue Mysql::Error
		puts "Error in creating connection"
		puts "Mysql service not run or wrong password"
		exit 1
	end

	id = 0
	numlove = 0
	begin
		insert_new_gag = db.prepare "REPLACE INTO gag (id,numlove,numcomment,numshare,numtweet,username,link) VALUES (?,?,?,?,?,?,?)"
		CSV.foreach("abc.csv") do |row|
			id = row[0]
			name = row[1]
			numlove_s = row[2]
			numlove = Integer numlove_s rescue 0
			numcomment = row[3]
			numshare_s = row[4]
			numshare_s = Integer numshare_s rescue numshare_s["k"]="000"
			numshare = Integer numshare_s
			numtweet = row[5]
			link = row[6]
			insert_new_gag.execute id,numlove,numcomment,numshare,numtweet,name,link
		end
		insert_new_gag.close
	rescue Mysql::Error
		puts id
		puts numlove
		puts numcomment
		puts numshare
		puts numtweet
	end

	# insert_new_gag = db.prepare "INSERT INTO gag (id,numlove,numcomment,numshare,numtweet,username,link) VALUES (?,?,?,?,?,?,?)"
	# insert_new_gag.execute "599006","-6","0","0","0","ohsum","http://d24w6bsrhbeh9d.cloudfront.net/photo/599006_700b.jpg"
	# insert_new_gag.close

	# statement = db.prepare "SELECT * FROM gag WHERE id = ?"
	# statement.execute '599006'
	# put statement.fetch
	# statement.close
ensure
	db.close
end

